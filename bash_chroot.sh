#!/bin/bash

echo "INICIANDO SCRIPT"
ROOT=/tmp/chroot
mkdir $ROOT/
rm -rf $ROOT/*

echo "MKDIR"
mkdir -p $ROOT/var/lib/rpm

echo "INITDB"
rpm --root $ROOT --initdb

echo "DOWNLOAD"
yum -y install yum-utils
yumdownloader --destdir=/tmp centos-release

echo "INSTALANDO SISTEMA"
rpm --root $ROOT -ivh --nodeps /tmp/centos-release*rpm

echo "INSTALANDO BIBLIOTECAS"
cp -a /etc/resolv.conf /tmp/chroot/etc/
yum --installroot=$ROOT -y install coreutils yum svn

echo "INSTALACAO VBWEB"
#cp -a /util/instalacao_vendabemweb $ROOT
#chroot $ROOT /instalacao_cendabemweb/instalaweb.sh

