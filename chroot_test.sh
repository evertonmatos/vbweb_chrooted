#echo "INICIANDO SCRIPT"
#ROOT=$1
#
#echo "MKDIR"
#mkdir -p $ROOT/var/lib/rpm
#
#echo "INITDB"
#rpm --root $ROOT --initdb
#
#echo "DOWNLOAD"
##dnf download --destdir=/tmp fedora-release
#yum download --destdir=/tmp centos-release
#
#echo "INSTALANDO SISTEMA"
#rpm --root $ROOT -ivh --nodeps /tmp/fedora-release*rpm #vai dar um erro, ignora por enquanto
#dnf --installroot=$ROOT -y install systemd
#
##echo "TRANSFORMANDO EM CHROOT"
#chroot $ROOT #ls

echo "INICIANDO SCRIPT"
ROOT=$1

echo "MKDIR"
mkdir -p $ROOT/var/lib/rpm

echo "INITDB"
rpm --root $ROOT --initdb

echo "DOWNLOAD"
yumdownloader --destdir=/tmp centos-release

echo "INSTALANDO SISTEMA"
rpm --root $ROOT -ivh --nodeps /tmp/centos-release*rpm

echo "INSTALANDO BIBLIOTECAS"
cp -a /etc/resolv.conf  /tmp/chroot/etc/
yum --installroot=$ROOT -y install coreutils yum

echo "INSTALACAO VBWEB"
cp -a /util/instalacao_vendabemweb /tmp/chroot/
chroot $ROOT /instalacao_vendabemweb/instalaweb.sh
